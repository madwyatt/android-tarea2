package desck.tk.tarea2;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;


public class MovieListFragment extends Fragment {

    RecyclerView recyclerView;
    MyMovieRecyclerViewAdapter adapter;
    private OnListFragmentInteractionListener mListener;

    public MovieListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        List<Movie> movieList = Collections.emptyList();
        adapter = new MyMovieRecyclerViewAdapter(movieList, mListener);

        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setMovieList(List<Movie> movieList) {
        adapter.setmValues(movieList);
        adapter.notifyDataSetChanged();
    }


    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Movie item, int position);
    }
}
