package desck.tk.tarea2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailsFragment extends Fragment {
    ImageView imageView;
    TextView title, id, year, type;

    public MovieDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
        imageView = view.findViewById(R.id.movie_poster);
        title = view.findViewById(R.id.title);
        id = view.findViewById(R.id.id);
        type = view.findViewById(R.id.type);
        year = view.findViewById(R.id.year);

        return view;
    }

    public void setMovie(Movie movie) {
        Picasso.get().load(movie.getImageUrl()).into(imageView);
        title.setText(movie.getTitle());
        id.setText("Id:   " + movie.getId());
        type.setText("Type: " + movie.getType());
        year.setText("Year: " + movie.getYear());

    }
}
