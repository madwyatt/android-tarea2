package desck.tk.tarea2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import desck.tk.tarea2.MovieListFragment.OnListFragmentInteractionListener;


public class MyMovieRecyclerViewAdapter extends RecyclerView.Adapter<MyMovieRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;
    private List<Movie> mValues;

    public MyMovieRecyclerViewAdapter(List<Movie> items, OnListFragmentInteractionListener listener) {
        mValues = Collections.emptyList();
        mListener = listener;
    }

    public void setmValues(List<Movie> mValues) {
        this.mValues = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_movie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.bind(mValues.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView textViewTitle;
        final ImageView textViewUrl;
        final View myview;

        ViewHolder(View view) {
            super(view);
            myview = view;
            textViewTitle = view.findViewById(R.id.item_number);
            textViewUrl = view.findViewById(R.id.content);
        }

        void bind(final Movie movie, final OnListFragmentInteractionListener listener) {
            this.textViewTitle.setText(movie.getTitle());
            Picasso.get()
                    .load(movie.getImageUrl())
                    .resize(240, 140)
                    .centerCrop()
                    .into(this.textViewUrl);
            myview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onListFragmentInteraction(movie, getAdapterPosition());
                }
            });
        }

    }

}
