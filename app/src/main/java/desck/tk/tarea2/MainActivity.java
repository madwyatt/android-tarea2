package desck.tk.tarea2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchFragment.OnFragmentInteractionListener, MovieListFragment.OnListFragmentInteractionListener {

    final String API_URL = "http://www.omdbapi.com/";
    List<Movie> movieList;
    MovieListFragment movieListFragment;
    MovieDetailsFragment movieDetailsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onListFragmentInteraction(Movie item, int position) {
        //Toast.makeText(this, item.toString(), Toast.LENGTH_SHORT).show();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment3);
        movieDetailsFragment = (MovieDetailsFragment) fragment;
        movieDetailsFragment.setMovie(item);
    }

    @Override
    public void onFragmentInteraction(String search) {

        String url = API_URL + "?s=" + search + "&apikey=fd00a37";
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024 * 2);
        Network network = new BasicNetwork(new HurlStack());
        RequestQueue requestQueue = new RequestQueue(cache, network);
        movieList = new ArrayList<>();

        requestQueue.start();

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray array = response.optJSONArray("Search");
                        try {
                            for (int i = 0; i < array.length(); i++) {
                                try {
                                    JSONObject object = array.getJSONObject(i);
                                    movieList.add(new Movie()
                                            .setId(object.getString("imdbID"))
                                            .setTitle(object.getString("Title"))
                                            .setType(object.getString("Type"))
                                            .setYear(object.getString("Year"))
                                            .setImageUrl(object.getString("Poster"))
                                    );
                                } catch (JSONException e) {
                                    Toast.makeText(MainActivity.this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment2);
                            movieListFragment = (MovieListFragment) fragment;
                            movieListFragment.setMovieList(movieList);
                        } catch (java.lang.NullPointerException e) {
                            Toast.makeText(MainActivity.this, "Not Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error: " + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        requestQueue.add(objectRequest);

    }
}
